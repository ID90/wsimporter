<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** AuthController */
Route::get('/', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login')->name('authorize');


Route::group(['middleware' => ['auth']], function () {
    /** DashboardController */
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/profile', 'DashboardController@profile')->name('profile');
    Route::post('/loader', 'DashboardController@loader')->name('loader');
    /** AuthController */
    Route::get('logout', 'AuthController@logout')->name('logout');
    /** RecordsController */
    Route::get('/records', 'RecordsController@index')->name('records');
    Route::get('/pending', 'RecordsController@pending')->name('pending');
    Route::get('/synced', 'RecordsController@synced')->name('synced');
    Route::get('/records/{record}', 'RecordsController@record')->name('record');
    /** WebsuiteController */
    Route::get('websuite/remove/{record}', 'WebsuiteController@remove')->name('websuite.remove.record');
    Route::get('websuite/all', 'WebsuiteController@all')->name('websuite.all');
    Route::get('websuite/{record}', 'WebsuiteController@record')->name('websuite.record');
    /** CsvController */
    Route::get('/csv/import', 'CsvController@show')->name('csv.form');
    Route::post('/csv/import', 'CsvController@store')->name('csv.import');
    /** AjaxController */
    Route::post('/sync/all', 'AjaxController@syncAllRecords');
    Route::post('/upload-csv', 'AjaxController@uploadCsv');
    Route::post('/process-csv', 'AjaxController@processCSV');
    Route::post('/remove-csv', 'AjaxController@removeCsvFiles');
});
