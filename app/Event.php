<?php

namespace App;

use App\Interfaces\RecordsInterface;
use Illuminate\Database\Eloquent\Model;

class Event extends Model implements RecordsInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * @param array $record
     *
     * @return $this
     */
    public function setRecord(array $record = [])
    {
        $this->obituary_id              = $record['ObituaryId'];
        $this->event_name               = $record['EventName'];
        $this->location_id              = $record['LocationId'];
        $this->location_name            = $record['LocationName'];
        $this->start_time               = $record['StartTime'];
        $this->end_time                 = $record['EndTime'];
        $this->phone                    = $record['Phone'];
        $this->email                    = $record['Email'];
        $this->website                  = $record['WebSite'];
        $this->address                  = $record['Address'];
        $this->city                     = $record['City'];
        $this->state                    = $record['State'];
        $this->country                  = $record['Country'];
        $this->postal_code              = $record['PostalCode'];
        $this->additional_service_info  = $record['AdditionalServiceInfo'];
        $this->is_memorial_contribution = ($record['IsMemorialContribution'] == "True") ? true : false;
        $this->show_on_website          = ($record['ShowOnWebSite'] == "True") ? true : false;

        return $this;
    }
}
