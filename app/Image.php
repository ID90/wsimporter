<?php

namespace App;

use App\Interfaces\RecordsInterface;
use Illuminate\Database\Eloquent\Model;

class Image extends Model implements RecordsInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * @param array $record
     * @return $this
     */
    public function setRecord(array $record = [])
    {
        $this->obituary_id        = $record['ObituaryId'];
        $this->image_path         = $record['ImagePath'];
        $this->description        = $record['Description'];
        $this->created_by         = $record['CreatedBy'];
        $this->created_on         = $record['CreatedOn'];
        $this->album_name         = $record['AlbumName'];
        $this->original_image_url = $record['OriginalImageUrl'];

        return $this;
    }
}
