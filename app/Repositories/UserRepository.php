<?php


namespace App\Repositories;


use App\Obituary;
use App\User;

class UserRepository
{
    /**
     * Check does record belong to current user
     *
     * @param int $userId
     * @param int $recordId
     *
     * @return bool
     */
    public function isOwnRecord(int $userId, int $recordId): bool
    {
        return Obituary::where([
            'customer_id' => $userId,
            'id'          => $recordId
        ])->exists();
    }

    public function getAllRecords(User $user)
    {
        return  Obituary::where([
            'customer_id' => $user->id
        ])->get();
    }

    public function recordsBySynced(User $user, bool $synced)
    {
        return Obituary::where([
            'customer_id' => $user->id,
            'synced'      => $synced
        ])->get();
    }

    public function countRecordsBySynced(User $user, bool $synced)
    {
        return Obituary::where([
            'customer_id' => $user->id,
            'synced'      => $synced
        ])->count();
    }

    public function countAllRecords(User $user)
    {
        return  Obituary::where([
            'customer_id' => $user->id
        ])->count();
    }

    public function chunkPendingRecords(User $user, int $take = 10)
    {
        return Obituary::where([
            'customer_id' => $user->id,
            'synced'      => false
        ])->take($take)->get();
    }


    /**
     * @param User $user
     * @param string $period
     * @param null $synced
     *
     * @return int
     */
    public function statAllRecordsByPeriod(User $user, string $period = 'month', $synced = null): int
    {
        $query = Obituary::where([
            'customer_id' => $user->id
        ]);

        if (isset($synced)) {
            $query = $query->where('synced', '=', $synced);
        }

        if ($period === 'today') {
            $query = $query
                ->whereDay('created_at', '=', date('d'))
                ->whereMonth('created_at', '=', date('m'))
                ->whereYear('created_at', '=', date('Y'));
        }

        if ($period === 'month') {
            $query = $query
                ->whereMonth('created_at', '=', date('m'))
                ->whereYear('created_at', '=', date('Y'));
        }

        if ($period === 'year') {
            $query = $query->whereYear('created_at', '=', date('Y'));
        }

        return $query->count();
    }
}

