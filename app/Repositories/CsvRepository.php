<?php


namespace App\Repositories;


use App\Comment;
use App\Event;
use App\Image;
use App\Obituary;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use League\Csv\Reader;

class CsvRepository
{
    /**
     * Save obituary data from csv file int database
     *
     * @param UploadedFile $file
     * @throws \League\Csv\Exception
     */
    public function saveObituaries(UploadedFile $file): void
    {
        $executionStartTime = microtime(true);

        $reader = Reader::createFromPath($file->getRealPath(), 'r');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();

        foreach ($records as $offset => $record) {
            $model = new Obituary();
            $model->setRecord($record);
            $model->save();
        }
        $executionEndTime = microtime(true);

        $seconds = $executionEndTime - $executionStartTime;
        Log::info('Uploading obituary.csv time sec: '.$seconds.' rows : '.$reader->count());
    }

    /**
     * Save event data from csv file int database
     *
     * @param UploadedFile $file
     * @throws \League\Csv\Exception
     */
    public function saveEvents(UploadedFile $file): void
    {
        $executionStartTime = microtime(true);

        $reader = Reader::createFromPath($file->getRealPath(), 'r');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();

        foreach ($records as $offset => $record) {
            $model = new Event();
            $model->setRecord($record);
            $model->save();
        }
        $executionEndTime = microtime(true);

        $seconds = $executionEndTime - $executionStartTime;
        Log::info('Uploading events.csv time sec: '.$seconds.' rows : '.$reader->count());
    }

    /**
     * Save comment data from csv file int database
     *
     * @param UploadedFile $file
     * @throws \League\Csv\Exception
     */
    public function saveComments(UploadedFile $file): void
    {
        $executionStartTime = microtime(true);

        $reader = Reader::createFromPath($file->getRealPath(), 'r');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();

        foreach ($records as $offset => $record) {
            $model = new Comment();
            $model->setRecord($record);
            $model->save();
        }
        $executionEndTime = microtime(true);

        $seconds = $executionEndTime - $executionStartTime;
        Log::info('Uploading comment.csv time sec: '.$seconds.' rows : '.$reader->count());
    }

    /**
     * Save image data from csv file int database
     *
     * @param UploadedFile $file
     * @throws \League\Csv\Exception
     */
    public function saveImages(UploadedFile $file): void
    {
        $executionStartTime = microtime(true);

        $reader = Reader::createFromPath($file->getRealPath(), 'r');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();

        foreach ($records as $offset => $record) {
            $model = new Image();
            $model->setRecord($record);
            $model->save();
        }

        $executionEndTime = microtime(true);

        $seconds = $executionEndTime - $executionStartTime;
        Log::info('Uploading comment.csv time sec: '.$seconds.' rows : '.$reader->count());
    }
}

