<?php

namespace App;

use App\Interfaces\RecordsInterface;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model implements RecordsInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * @param array $record
     * @return $this
     */
    public function setRecord(array $record = [])
    {
        $this->parent_id       = $record['ParentId'];
        $this->obituary_id     = $record['ObituaryId'];
        $this->from_name       = $record['FromName'];
        $this->message         = $record['Message'];
        $this->date            = $record['Date'];
        $this->candle          = $record['Candle'];
        $this->gift            = $record['Gift'];
        $this->image_thumbnail = $record['ImageThumbnail'];
        $this->comment_type    = $record['CommentType'];
        $this->heart_name      = $record['HeartName'];
        $this->album_id        = $record['AlbumId'];

        return $this;
    }
}
