<?php


namespace App\Interfaces;


interface RecordsInterface
{
    public function setRecord(array $record);
}
