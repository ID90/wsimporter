<?php


namespace App\Helpers;


class CsvHelper
{
    const ROWS_PER_ITERATION = 50;

    const TYPE_OBITUARY = 'obituary';
    const TYPE_EVENT    = 'event';
    const TYPE_COMMENT  = 'comment';
    const TYPE_IMAGE    = 'image';

    const IMPORTING_CSV = [
        self::TYPE_OBITUARY,
        self::TYPE_EVENT,
        self::TYPE_COMMENT,
        self::TYPE_IMAGE
    ];
}
