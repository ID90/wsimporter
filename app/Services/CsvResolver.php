<?php


namespace App\Services;


use App\Comment;
use App\Event;
use App\Helpers\CsvHelper;
use App\Image;
use App\Obituary;

class CsvResolver
{
    public $instance;

    /**
     * CsvResolver constructor.
     *
     * @param string $type
     */
    public function __construct(string $type)
    {
        switch ($type) {
            case CsvHelper::TYPE_OBITUARY:
                $this->instance = new Obituary();
                break;
            case CsvHelper::TYPE_EVENT:
                $this->instance = new Event();
                break;
            case CsvHelper::TYPE_COMMENT:
                $this->instance = new Comment();
                break;

            case CsvHelper::TYPE_IMAGE:
                $this->instance = new Image();
                break;
        }

        return $this->instance;
    }

    public  function getInstance()
    {
        return $this->instance;
    }
}
