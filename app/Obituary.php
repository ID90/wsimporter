<?php

namespace App;

use App\Interfaces\RecordsInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Obituary extends Model implements RecordsInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'obituaries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'synced',
        'guid'
    ];

    /**
     * @param array $record
     * @return $this
     */
    public function setRecord(array $record = [])
    {
        $user = Auth::user();

        $this->external_id            = $record['Id'];
        $this->customer_id            = $user->id;
        $this->guid                   = '';
        $this->first_name             = $record['FirstName'];
        $this->middle_name            = $record['MiddleName'];
        $this->last_name              = $record['LastName'];
        $this->birth_date             = Carbon::parse($record['BirthDate'])->format('Y-m-d');
        $this->death_date             = Carbon::parse($record['DeathDate'])->format('Y-m-d');
        $this->description            = $record['Description'];
        $this->video_link             = $record['VideoLink'];
        $this->video_in_comment       = ($record['VideoInComment'] == 'True') ? true : false;
        $this->image_path             = $record['ImagePath'];
        $this->place_of_residence     = $record['PlaceOfResidence'];
        $this->serving_location_id    = $record['ServingLocationId'];
        $this->serving_location_name  = $record['ServingLocationName'];

        return $this;
    }
}
