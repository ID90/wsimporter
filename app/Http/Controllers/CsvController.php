<?php

namespace App\Http\Controllers;

use App\Repositories\CsvRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CsvController extends Controller
{
    /**
     * @var CsvRepository
     */
    public $repository;

    /**
     * CsvController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->repository = new CsvRepository();
    }

    public function show()
    {
        return view('csv/uploader');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $this->repository->saveObituaries($request->file('obituary'));
            $this->repository->saveEvents($request->file('event'));
            $this->repository->saveComments($request->file('comment'));
            $this->repository->saveImages($request->file('image'));

        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', $e->getMessage());
        }

        DB::commit();

        return back()->with('success','CSVs data inserted successfully!');
    }
}
