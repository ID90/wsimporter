<?php

namespace App\Http\Controllers;

use App\Obituary;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WebsuiteController extends Controller
{
    /**
     * WebsuiteController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Sync single record into WebSuite
     *
     * @param Obituary $record
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function record(Obituary $record)
    {
        $guid = Str::uuid()->toString();
        $user = Auth::user();

        DB::beginTransaction();

        try {
            $xml = view('xml/deceased-info-max')->with([
                'guid'        => $guid,
                'partner_id'  => $user->partner_id,
                'customer_id' => $user->customer_id,
                'first_name'  => $record->first_name,
                'middle_name' => $record->middle_name,
                'last_name'   => $record->last_name,
                'death_date'  => $record->death_date,
                'birth_date'  => $record->birth_date,
                'obituary'    => $record->description,
            ])->render();

            $client = new Client();
            $options = [
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8',
                ],
                'body' => $xml,
            ];

            $client->request('POST', env('WEBSUITE_API_URL'), $options);

            $record->guid   = $guid;
            $record->synced = true;
            $record->save();

        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', 'Oops! Something went wrong with record synchronization.');
        }

        DB::commit();
        return back()->with('success','Record was synchronized successfully into WebSuite.');
    }

    /**
     * Remove record from WebSuite
     *
     * @param Obituary $record
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function remove(Obituary $record)
    {
        $user = Auth::user();

        DB::beginTransaction();

        try {
            $xml = view('xml/remove-record')->with([
                'guid'        => $record->guid,
                'partner_id'  => $user->partner_id,
                'customer_id' => $user->customer_id
            ])->render();

            $client = new Client();
            $options = [
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8',
                ],
                'body' => $xml,
            ];

            $client->request('DELETE', env('WEBSUITE_API_URL'), $options);

            $record->guid   = '';
            $record->synced = false;
            $record->save();

        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', 'Oops! Something went wrong with record deleting.');
        }

        DB::commit();
        return back()->with('success','Record was deleted successfully from WebSuite.');
    }

    public function all()
    {
        $user = Auth::user();
        $records = Obituary::where([
            'customer_id' => $user->id,
            'synced'      => false
        ])->get();

        $count = $records->count();

        if (!$count) {
            return back()->with('warning', 'All record are synchronized already.');
        }

        DB::beginTransaction();

        try {
            foreach ($records as $record) {

                $guid = Str::uuid()->toString();

                $xml = view('xml/deceased-info-max')->with([
                    'guid'        => $guid,
                    'partner_id'  => $user->partner_id,
                    'customer_id' => $user->customer_id,
                    'first_name'  => $record->first_name,
                    'middle_name' => $record->middle_name,
                    'last_name'   => $record->last_name,
                    'death_date'  => $record->death_date,
                    'birth_date'  => $record->birth_date,
                    'obituary'    => $record->description,
                ])->render();

                $client = new Client();
                $options = [
                    'headers' => [
                        'Content-Type' => 'text/xml; charset=UTF8',
                    ],
                    'body' => $xml,
                ];

                $client->request('POST', env('WEBSUITE_API_URL'), $options);

                $record->guid   = $guid;
                $record->synced = true;
                $record->save();
            }

        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', 'Oops! Something went wrong with record synchronization.');
        }

        DB::commit();
        return back()->with('success','All records was synchronized successfully into WebSuite.');
    }
}
