<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * @var UserRepository
     */
    public $userRepository;

    const STAT_PERIOD = [
      'today' => 'Today',
      'month' => 'This Month',
      'year'  => 'This Year',
      'all'   => 'All Time',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->userRepository = new UserRepository();
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $period = $request->get('period') ?? 'all';

        $total   = $this->userRepository->statAllRecordsByPeriod($user, $period);
        $synced  = $this->userRepository->statAllRecordsByPeriod($user, $period,true);
        $pending = $this->userRepository->statAllRecordsByPeriod($user, $period, false);

        return view('dashboard/index', [
            'total'   => $total,
            'pending' => $pending,
            'synced'  => $synced,
            'label'   => self::STAT_PERIOD[$period],
            'active'  => $period
        ]);
    }

    public function profile()
    {
        $user = Auth::user();

        return view('dashboard/profile', ['user' => $user]);
    }

    public function loader(Request $request)
    {
        if ($request->ajax()) {

            $user = Auth::user();

            return [
                'success' => true,
                'data'   => [
                    'total'   => $this->userRepository->statAllRecordsByPeriod($user, 'all'),
                    'synced'  => $this->userRepository->statAllRecordsByPeriod($user, 'all',true),
                    'pending' => $this->userRepository->statAllRecordsByPeriod($user, 'all', false)
                ]
            ];
        }

        return redirect()->to('dashboard');
    }
}

