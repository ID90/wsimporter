<?php

namespace App\Http\Controllers;

use App\Helpers\CsvHelper;
use App\Obituary;
use App\Repositories\UserRepository;
use App\Services\CsvResolver;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Csv\Reader;
use League\Csv\Statement;

class AjaxController extends Controller
{

    /**
     * @var UserRepository
     */
    public $userRepository;

    /**
     * AjaxController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->userRepository = new UserRepository();
    }

    public function syncAllRecords(Request $request)
    {
        if ($request->ajax()) {

            $user    = Auth::user();
            $records = $this->userRepository->chunkPendingRecords($user);
            $total   = $this->userRepository->countRecordsBySynced($user, false);

            if (!$total) {
                return 'done';
            }

            $client = new Client();

            foreach ($records as $record) {

                $guid = Str::uuid()->toString();
                $xml = view('xml/deceased-info-max')->with([
                    'guid'        => $guid,
                    'partner_id'  => $user->partner_id,
                    'customer_id' => $user->customer_id,
                    'first_name'  => $record->first_name,
                    'middle_name' => $record->middle_name,
                    'last_name'   => $record->last_name,
                    'death_date'  => $record->death_date,
                    'birth_date'  => $record->birth_date,
                    'obituary'    => $record->description,
                ])->render();

                $options = [
                    'headers' => [
                        'Content-Type' => 'text/xml; charset=UTF8',
                    ],
                    'body' => $xml,
                ];

                $client->request('POST', env('WEBSUITE_API_URL'), $options);

                $record->guid   = $guid;
                $record->synced = true;
                $record->save();
            }

            return 'in_progress';
        }

        return redirect()->to('dashboard');
    }


    public function uploadCsv(Request $request)
    {
        if ($request->ajax()) {
            $batch = time();
            Storage::putFileAs('csv', new File($request->obituary), "obituary_$batch.csv");
            Storage::putFileAs('csv', new File($request->event), "event_$batch.csv");
            Storage::putFileAs('csv', new File($request->comment), "comment_$batch.csv");
            Storage::putFileAs('csv', new File($request->image), "image_$batch.csv");

            return ['batch' => $batch];
        }

        return redirect()->to('dashboard');
    }

    public function processCSV(Request $request)
    {
        if ($request->ajax()) {

            $type   = $request->type;
            $offset = $request->offset;
            $batch  = $request->batch;

            if (!in_array($type, CsvHelper::IMPORTING_CSV)) {
                return response()->json(['error' => 'Wrong CSV file type'])->setStatusCode(400);
            }

            $filePath = storage_path("app/csv/".$type."_".$batch.".csv");

            $reader = Reader::createFromPath($filePath, 'r');
            $reader->setHeaderOffset(0);
            $count = $reader->count();

            $stmt = (new Statement())
                ->offset($offset)
                ->limit(CsvHelper::ROWS_PER_ITERATION);

            $records = $stmt->process($reader);

            foreach ($records as $record) {
                $model = (new CsvResolver($type))->getInstance();
                $model->setRecord($record);
                $model->save();
            }

            if ($offset > $count) {
                $nextType = $this->resolveNextType($type, CsvHelper::IMPORTING_CSV);
                $response = ($nextType) ?
                    [
                        'type'   => $nextType,
                        'offset' => 0,
                        'status' => 'processing'
                    ] :
                    ['status' => 'done'];
            } else {
                $response = [
                    'type'   => $type,
                    'offset' => $offset + CsvHelper::ROWS_PER_ITERATION,
                    'status' => 'processing'
                ];
            }

            $response['batch'] = $batch;

            return response()->json($response);
        }

        return redirect()->to('dashboard');
    }

    public function removeCsvFiles(Request $request)
    {
        if ($request->ajax()) {

            $batch = $request->batch;

            foreach (CsvHelper::IMPORTING_CSV as $type) {
                // Remove csv files after importing
                Storage::delete("csv/".$type."_".$batch.".csv");
            }

            return ['success' => true];
        }
        return redirect()->to('dashboard');
    }

    public function resolveNextType(string $current, array $haystack)
    {
        $currentKey = array_search($current, $haystack);

        $nextKey = $currentKey + 1;

        if ($nextKey >= count($haystack)) {
            return false;
        }

        return $haystack[$nextKey];
    }
}
