<?php

namespace App\Http\Controllers;

use App\Obituary;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecordsController extends Controller
{
    /**
     * @var UserRepository
     */
    public $userRepository;

    /**
     * RecordsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->userRepository = new UserRepository();
    }

    /**
     * Get all list of records that was imported from CSV
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $query = Obituary::where([
            'customer_id' => $user->id
        ]);

        if ($search = $request->get('search')) {
            $query = $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE' , '%'.$search.'%');
        }

        $records = $query->paginate(20);
        $pending = $this->userRepository->statAllRecordsByPeriod($user, 'all', false);

        return view(
            'records/index', [
                'pending' => $pending,
                'records' => $records,
                'count'   => $query->count()
            ]
        );
    }

    /**
     * Get pending list of records that was imported from CSV
     */
    public function pending()
    {
        $user = Auth::user();

        $query = Obituary::where([
            'customer_id' => $user->id,
            'synced'      => false
        ]);

        $records = $query->paginate(20);

        $pending = $this->userRepository->statAllRecordsByPeriod($user, 'all', false);

        return view(
            'records/index', [
                'pending' => $pending,
                'records' => $records,
                'count'   => $query->count()
            ]
        );
    }

    /**
     * Get pending list of records that was imported from CSV
     */
    public function synced()
    {
        $user = Auth::user();

        $query = Obituary::where([
            'customer_id' => $user->id,
            'synced'      => true
        ]);

        $records = $query->paginate(20);

        $pending = $this->userRepository->statAllRecordsByPeriod($user, 'all', false);

        return view(
            'records/index', [
                'pending' => $pending,
                'records' => $records,
                'count'   => $query->count()
            ]
        );
    }

    public function record(Obituary $record)
    {
        if (!$this->userRepository->isOwnRecord(Auth::user()->id, $record->id)) {
            return abort(404);
        }

        return view('records/record', ['record' => $record]);
    }
}
