<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    CONST TEST_NAME = 'TEST';
    CONST TEST_DATE = '2020-01-01';

    public function index()
    {
        if (Auth::check()) {
            return redirect()->to('/dashboard');
        }

        return view('auth/index');
    }

    /**
     * @todo need refactoring
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function login(Request $request)
    {
        request()->validate([
            'partner_id'  => 'required',
            'customer_id' => 'required',
        ]);

        $credentials = $request->only('partner_id', 'customer_id');

        $user = User::where([
            'partner_id'  => $credentials['partner_id'],
            'customer_id' => $credentials['customer_id'],
            'active'      => true
        ])->first();


        if ($user !== null) {
            Auth::login($user);
            return redirect()->to('dashboard');
        }

        try {
            // Register user with credentials
            $guid = Str::uuid()->toString();
            $xml = view('xml/deceased-info')->with([
                'guid'        => $guid,
                'partner_id'  => $credentials['partner_id'],
                'customer_id' => $credentials['customer_id'],
                'first_name'  => self::TEST_NAME,
                'last_name'   => self::TEST_NAME,
                'death_date'  => self::TEST_DATE,
            ])->render();

            $client = new Client();
            $options = [
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8',
                ],
                'body' => $xml,
            ];

            $client->request('POST', env('WEBSUITE_API_URL'), $options);

            $xml = view('xml/remove-record')->with([
                'guid'        => $guid,
                'partner_id'  => $credentials['partner_id'],
                'customer_id' => $credentials['customer_id'],
            ])->render();

            $options = [
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8',
                ],
                'body' => $xml,
            ];

            $client = new Client();
            $client->request('DELETE', env('WEBSUITE_API_URL'), $options);

            $user = new User();
            $user->partner_id  = $credentials['partner_id'];
            $user->customer_id = $credentials['customer_id'];
            $user->save();

            Auth::login($user);
            return redirect()->to('dashboard');

        } catch (\Exception $e) {
            return back()->with('error', 'Incorrect Partner ID or Customer ID.');
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->to('/');
    }
}
