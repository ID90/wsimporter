@extends('layouts.app')

@section('title', 'Import CSV')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Import CSV</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Import CSV</li>
        </ol>
    </div>


    <div class="card">
        <div class="card-body">

            <div class="message-board">
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h6><i class="fas fa-info-circle"></i><b> Important !</b></h6>
                    Choose your 4 csv files according the file names that specified below !
                </div>
            </div>

            <form class="md-form" id="formCSV" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="form-group">
                    <label for="obituary" class="custom-file-upload btn-success">
                        <i class="fa fa-cloud-upload"></i> Choose your <b>obituary.csv</b>
                    </label>
                    <input id="obituary" type="file" name="obituary"/>
                </div>
                <div class="form-group">
                    <label for="event" class="custom-file-upload btn-success">
                        <i class="fa fa-cloud-upload"></i> Choose your <b>event.csv</b>
                    </label>
                    <input id="event" type="file" name="event"/>
                </div>
                <div class="form-group">
                    <label for="comment" class="custom-file-upload btn-success">
                        <i class="fa fa-cloud-upload"></i> Choose your <b>comment.csv</b>
                    </label>
                    <input id="comment" type="file" name="comment"/>
                </div>
                <div class="form-group">
                    <label for="image" class="custom-file-upload btn-success">
                        <i class="fa fa-cloud-upload"></i> Choose your <b>image.csv</b>
                    </label>
                    <input id="image" type="file" name="image"/>
                </div>


                <button type="submit" class="btn btn-info submitCSV"data-toggle="modal"
                        data-target="#loaderModalCenter"
                        id="#modalCenter">Submit
                </button>
            </form>

        </div>
        <div class="card-footer"></div>
    </div>

@endsection

<style>
    input[type="file"] {
        display: none;
    }
    .custom-file-upload {
        text-align: center;
        width: 300px;
        border: 1px solid #ccc;
        display: inline-block;
        padding: 6px 12px;
        cursor: pointer;
    }
</style>

