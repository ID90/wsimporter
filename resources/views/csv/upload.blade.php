@extends('layouts.app')

@section('title', 'Profile')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Import CSV</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Import CSV</li>
        </ol>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif


    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="card">
        <div class="card-body">

            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h6><i class="fas fa-info-circle"></i><b> Important !</b></h6>
                Choose your 4 csv files according the file names that specified below !
            </div>

            <form action="{{route('csv.import')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="input-group input-file" name="obituary">
                        <label for="obituary" class="col-sm-2 col-form-label"><b>obituary.csv</b></label>
                        <input type="text" class="form-control col-sm-8" id="obituary" required placeholder='Choose a obituary.csv file...'/>
                        <span class="input-group-btn col-sm-2">
                        <button class="btn btn-info btn-choose" type="button">Choose</button>
                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-file" name="event">
                        <label for="event" class="col-sm-2 col-form-label"><b>event.csv</b></label>
                        <input type="text" class="form-control col-sm-8" id="event" required placeholder='Choose a event.csv file...'/>
                        <span class="input-group-btn col-sm-2">
                        <button class="btn btn-info btn-choose" type="button">Choose</button>
                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-file" name="comment">
                        <label for="comment" class="col-sm-2 col-form-label"><b>comment.csv</b></label>
                        <input type="text" class="form-control col-sm-8" id="comment" required placeholder='Choose a comment.csv file...'/>
                        <span class="input-group-btn col-sm-2">
                        <button class="btn btn-info btn-choose" type="button">Choose</button>
                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-file" name="image">
                        <label for="image" class="col-sm-2 col-form-label"><b>image.csv</b></label>
                        <input type="text" class="form-control col-sm-8" id="image" required placeholder='Choose a image.csv file...'/>
                        <span class="input-group-btn col-sm-2">
                        <button class="btn btn-info btn-choose" type="button">Choose</button>
                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-form-label"></label>
                    <button type="submit" class="btn btn-success pull-right" >Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
            </form>
        </div>

        <div class="card-footer"></div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>

<script type="text/javascript">
    function bs_input_file() {
        $(".input-file").before(
            function () {
                if (!$(this).prev().hasClass('input-ghost')) {
                    var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                    element.attr("name", $(this).attr("name"));
                    element.change(function () {
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                    });
                    $(this).find("button.btn-choose").click(function () {
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function () {
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor", "pointer");
                    $(this).find('input').mousedown(function () {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }

    $(function () {
        bs_input_file();
    });
</script>
