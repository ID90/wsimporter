@extends('layouts.app')

@section('title', 'Profile')

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Profile</li>
    </ol>
</div>

<div class="card">

    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">General Information</h6>
    </div>

    <div class="card-body">
        <form>
            <div class="form-group">
                <label for="partner_id">Partner ID</label>
                <input type="text" class="form-control is-valid" id="partner_id" value="{{$user->partner_id}}" readonly="" required="">
                <div class="valid-feedback">
                    Verified!
                </div>
            </div>

            <div class="form-group">
                <label for="customer_id">Customer ID</label>
                <input type="text" class="form-control is-valid" id="customer_id" value="{{$user->customer_id}}" readonly="" required="">
                <div class="valid-feedback">
                    Verified!
                </div>
            </div>

            <div class="form-group">
                <label for="created">Created</label>
                <input type="text" class="form-control" id="created" value="{{$user->created_at}}" readonly="" required="">
            </div>
        </form>
    </div>

</div>

@endsection
