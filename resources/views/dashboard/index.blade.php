@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-uppercase mb-1">All imported records</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-battery-full fa-2x text-primary"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-uppercase mb-1">Synchronized</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$synced}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-sync fa-2x text-success"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 mb-4">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-uppercase mb-1">Pending</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$pending}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-pause-circle fa-2x text-warning"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div><!--row-->

            <div class="row">
                <div class="col-xl-4 col-lg-5">
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Progress View</h6>
                            <div class="dropdown no-arrow">
                                <a class="dropdown-toggle btn btn-primary btn-sm" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$label}} <i class="fas fa-chevron-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Select Period</div>
                                    <a class="dropdown-item {{($active == 'today') ? 'active' : ''}}" href="/dashboard?period=today">Today</a>
                                    <a class="dropdown-item {{($active == 'month') ? 'active' : ''}}" href="/dashboard?period=month">This Month</a>
                                    <a class="dropdown-item {{($active == 'year') ? 'active' : ''}}" href="/dashboard?period=year">This Year</a>
                                    <a class="dropdown-item {{($active == 'all') ? 'active' : ''}}" href="/dashboard">All Time</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="mb-3">
                                <div class="small text-gray-500">All imported records
                                    <div class="small float-right"><b>{{$total}} Records</b></div>
                                </div>
                                <div class="progress" style="height: 12px;">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: {{$total ? 100 : 0}}%" aria-valuenow="{{$total ? 100 : 0}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="small text-gray-500">Synchronized
                                    <div class="small float-right"><b>{{$synced}} of {{$total}} Records</b></div>
                                </div>
                                <div class="progress" style="height: 12px;">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{(!$total) ? 0 : ($synced*100)/$total}}%" aria-valuenow="{{(!$total) ? 0 : ($synced*100)/$total}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="small text-gray-500">Pending
                                    <div class="small float-right"><b>{{$pending}} of {{$total}} Records</b></div>
                                </div>
                                <div class="progress" style="height: 12px;">
                                    <div class="progress-bar bg-warning" role="progressbar" style="width: {{(!$total) ? 0 : ($pending*100)/$total}}%" aria-valuenow="{{(!$total) ? 0 : ($pending*100)/$total}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->

        </div>
        <div class="card-footer"></div>
    </div>
@endsection
