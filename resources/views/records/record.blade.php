@extends('layouts.app')

@section('title', 'Profile')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detailed information</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('records')}}">Records</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$record->first_name }} {{$record->last_name }}</li>
        </ol>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="card">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Information</h6>
        </div>

        <div class="card-body">
            @if($record->synced)
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h6><i class="fas fa-check"></i><b> Success!</b></h6>
                    This record is synchronized already!
                </div>
            @else
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h6><i class="fas fa-exclamation-triangle"></i><b> Notice!</b></h6>
                    This record is not synchronized yet!
                </div>
            @endif

            <form>
                <div class="form-group row">
                    <label for="id" class="col-sm-2 col-form-label"><b>ID</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="id" value="{{ $record->id }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="full_name" class="col-sm-2 col-form-label"><b>Full Name</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="full_name"
                               value="{{ $record->first_name }} {{ $record->middle_name }} {{ $record->last_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="birth_date" class="col-sm-2 col-form-label"><b>Birth Date</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="birth_date"
                               value="{{ $record->birth_date }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="death_date" class="col-sm-2 col-form-label"><b>Death Date</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="death_date"
                               value="{{ $record->death_date }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label"><b>Description</b></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" readonly id="description"
                                  rows="8">{{ $record->description }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label"><b>Status</b></label>
                    <div class="col-sm-10">
                        @if($record->synced)
                            <a href="#" class="btn btn-success btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-check"></i>
                            </span>
                            <span class="text">Synchronized in WebSuite</span>
                            </a>
                        @else
                            <a href="#" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-exclamation-triangle"></i>
                            </span>
                            <span class="text">Not synchronized in WebSuite</span>
                            </a>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="imported" class="col-sm-2 col-form-label"><b>Imported</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="imported"
                               value="{{ $record->created_at }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="video_link" class="col-sm-2 col-form-label"><b>Video Link</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="video_link"
                               value="{{ $record->video_link }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="video_in_comment" class="col-sm-2 col-form-label"><b>Video in comment</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="video_in_comment"
                               value="{{ $record->video_in_comment ? 'Yes' : 'No' }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="image_path" class="col-sm-2 col-form-label"><b>Image Path</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="image_path"
                               value="{{ $record->image_path }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="place_of_residence" class="col-sm-2 col-form-label"><b>Place of residence</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="place_of_residence"
                               value="{{ $record->place_of_residence }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="location_name" class="col-sm-2 col-form-label"><b>Serving location Name</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="location_name"
                               value="{{ $record->serving_location_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="location_id" class="col-sm-2 col-form-label"><b>Serving location ID</b></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="location_id"
                               value="{{ $record->serving_location_id }}">
                    </div>
                </div>

                @if(!$record->synced)
                <div class="form-group row">
                    <label for="location_id" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <a href="{{route('websuite.record', $record)}}" class="btn btn-info btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-sync"></i>
                            </span>
                            <span class="text">Synchronize into WebSuite</span>
                        </a>
                    </div>
                </div>
                @else
                <div class="form-group row">
                    <label for="location_id" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <a href="{{route('websuite.remove.record', $record)}}" class="btn btn-danger btn-icon-split">
                        <span class="icon text-white-50">
                          <i class="fas fa-undo"></i>
                        </span>
                            <span class="text">Remove from WebSuite</span>
                        </a>
                    </div>
                </div>
                @endif

            </form>
        </div>

        <div class="card-footer"></div>
    </div>
@endsection
