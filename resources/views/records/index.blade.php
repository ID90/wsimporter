@extends('layouts.app')

@section('title', 'Records list')

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Records list</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Records</li>
    </ol>
</div>

<div class="card">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        @if($pending)
            <button type="button" class="btn btn-info btn-icon-split syncall"
                    data-toggle="modal"
                    data-target="#loaderModalCenter"
                    id="#modalCenter">
                <span class="icon text-white-50">
                  <i class="fas fa-sync"></i>
                </span>
                <span class="text">Synchronize all records into WebSuite</span>
            </button>

        @endif
    </div>

    @if ($message = Session::get('warning'))
        <div class="card-body">
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="card-body">
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        </div>
    @endif


    @if ($message = Session::get('error'))
        <div class="card-body">
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        </div>
    @endif

    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Birth Date</th>
                    <th>Death Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($records as $record)
                <tr>
                    <td><a href="{{route('record', $record)}}">{{ $record->id }}</a></td>
                    <td>{{ $record->first_name }}</td>
                    <td>{{ $record->last_name }}</td>
                    <td>{{ $record->birth_date }}</td>
                    <td>{{ $record->death_date }}</td>
                    <td>
                        @if($record->synced)
                            <span class="badge badge-success">Delivered</span>
                        @else
                            <span class="badge badge-warning">Pending</span>
                        @endif
                    </td>
                    <td><a href="{{route('record', $record)}}" class="btn btn-sm btn-primary">Detail</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer"></div>

    <div class="col-sm-12 col-md-7">
        <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
            {{$records->links()}}
        </div>
    </div>

</div>

@endsection
