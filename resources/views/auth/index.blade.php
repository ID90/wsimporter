<!DOCTYPE html>
<html>
<head>
    <title>FrontRunnerPro</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{url('admin/img/logo/frontrunner-logo.png')}}" rel="icon">

    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{url('style.css')}}">

</head>
<body>
<div class="container-fluid">
    <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
        <div class="col-md-8 col-lg-6">
            <div class="login d-flex align-items-center py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-lg-8 mx-auto">
                            <h3 class="login-heading mb-4">Welcome back!</h3>

                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif

                            <form action="{{route('authorize')}}" method="POST" id="logForm">

                                {{ csrf_field() }}

                                <div class="form-label-group">
                                    <input type="text" name="partner_id" id="partner_id" class="form-control" required placeholder="Partner ID">
                                    <label for="partner_id">Partner ID</label>

                                    @if ($errors->has('partner_id'))
                                        <span class="error">{{ $errors->first('partner_id') }}</span>
                                    @endif
                                </div>

                                <div class="form-label-group">
                                    <input type="text" name="customer_id" id="customer_id" class="form-control" required placeholder="Customer ID">
                                    <label for="customer_id">Customer ID</label>

                                    @if ($errors->has('customer_id'))
                                        <span class="error">{{ $errors->first('customer_id') }}</span>
                                    @endif
                                </div>

                                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign In</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
