<api:record xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:api="http://websuite.frontrunnerpro.com/recscrapi/0.1/schema/frprecordscrape"
            xsi:schemaLocation="http://websuite.frontrunnerpro.com/recscrapi/0.1/schema/frprecordscrape http://websuite.frontrunnerpro.com/recscrapi/0.1/schema/frprecordscrape.xsd">
    <api:key>
        <api:webRecordId>{{$guid}}</api:webRecordId>
        <api:partnerId>{{$partner_id}}</api:partnerId>
        <api:customerId>{{$customer_id}}</api:customerId>
    </api:key>
    <api:deceasedInfo>
        <api:givenName>{{$first_name}}</api:givenName>
        <api:middleName>{{$middle_name}}</api:middleName>
        <api:lastName>{{$last_name}}</api:lastName>
{{--        <api:fileNumber></api:fileNumber>--}}
        <api:suffix></api:suffix>
        <api:nickName></api:nickName>
        <api:maidenName></api:maidenName>
{{--        <api:gender></api:gender>--}}
        <api:dateOfBirth>{{$birth_date}}</api:dateOfBirth>
        <api:dateOfDeath>{{$death_date}}</api:dateOfDeath>
{{--        <api:placeOfDeath>--}}
{{--            <api:name></api:name>--}}
{{--            <api:address>--}}
{{--                <api:address1></api:address1>--}}
{{--                <api:address2></api:address2>--}}
{{--                <api:city></api:city>--}}
{{--                <api:state></api:state>--}}
{{--                <api:country></api:country>--}}
{{--                <api:zip></api:zip>--}}
{{--            </api:address>--}}
{{--            <api:extraInfo></api:extraInfo>--}}
{{--        </api:placeOfDeath>--}}
        <api:placeOfDeathExtraInfo></api:placeOfDeathExtraInfo>
        <api:lifeStory></api:lifeStory>
        <api:obituary>{{$obituary}}</api:obituary>
    </api:deceasedInfo>
</api:record>
