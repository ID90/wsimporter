<api:record xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:api="http://websuite.frontrunnerpro.com/recscrapi/0.1/schema/frprecordscrape"
            xsi:schemaLocation="http://websuite.frontrunnerpro.com/recscrapi/0.1/schema/frprecordscrape http://websuite.frontrunnerpro.com/recscrapi/0.1/schema/frprecordscrape.xsd">
    <api:key>
        <api:webRecordId>{{$guid}}</api:webRecordId>
        <api:partnerId>{{$partner_id}}</api:partnerId>
        <api:customerId>{{$customer_id}}</api:customerId>
    </api:key>
    <api:deceasedInfo>
        <api:givenName>{{$first_name}}</api:givenName>
        <api:lastName>{{$last_name}}</api:lastName>
        <api:dateOfDeath>{{$death_date}}</api:dateOfDeath>
    </api:deceasedInfo>
</api:record>
