Frontrunner WebSuite Importer


## How to install

In your remote server you should use this commands on project's root repository.

git remote add origin git@bitbucket.org:ID90/wsimporter.git

git pull origin origin master


composer install 


## Configure your DataBase connection
Choose root file .env

**Put your configurations**

DB_CONNECTION=mysql

DB_HOST=

DB_PORT=

DB_DATABASE=

DB_USERNAME=

DB_PASSWORD=


## Run project migrations
php artisan migrate

## Finally
Add this information into file .env

WEBSUITE_API_URL=http://websuite.frontrunnerpro.com/recscrapi/0.1/record/

LOG_SLACK_WEBHOOK_URL=https://hooks.slack.com/services/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

## This is it
