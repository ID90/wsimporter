<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('obituary_id')->unsigned();
            $table->foreign('obituary_id')->references('external_id')->on('obituaries')->onDelete('cascade');
            $table->string('event_name');
            $table->string('location_id');
            $table->string('location_name');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('phone');
            $table->string('email');
            $table->string('website');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('postal_code');
            $table->string('additional_service_info');
            $table->boolean('is_memorial_contribution')->default(false);
            $table->boolean('show_on_website')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
