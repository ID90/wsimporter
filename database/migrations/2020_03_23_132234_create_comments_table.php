<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id');
            $table->bigInteger('obituary_id')->unsigned();
            $table->foreign('obituary_id')->references('external_id')->on('obituaries')->onDelete('cascade');
            $table->string('from_name');
            $table->longText('message');
            $table->string('date');
            $table->string('candle');
            $table->string('gift');
            $table->string('image_thumbnail');
            $table->string('comment_type');
            $table->string('heart_name');
            $table->string('album_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
