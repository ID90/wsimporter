<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObituariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obituaries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('external_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->unique(['external_id', 'customer_id', 'birth_date']);
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('synced')->default(false);
            $table->string('guid')->nullable();
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->date('birth_date');
            $table->date('death_date');
            $table->longText('description');
            $table->string('video_link');
            $table->boolean('video_in_comment')->default(false);
            $table->string('image_path');
            $table->string('place_of_residence');
            $table->string('serving_location_name');
            $table->string('serving_location_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obituaries');
    }
}
