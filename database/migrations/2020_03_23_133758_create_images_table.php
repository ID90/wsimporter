<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('obituary_id')->unsigned();
            $table->foreign('obituary_id')->references('external_id')->on('obituaries')->onDelete('cascade');
            $table->string('image_path');
            $table->longText('description');
            $table->string('created_by');
            $table->string('created_on');
            $table->string('album_name');
            $table->string('original_image_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
