$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".syncall").click(function (e) {
    e.preventDefault();
    recursively_sync_all_records()
});

$('#formCSV').on('submit',function (e) {
    e.preventDefault();

    formdata = new FormData();

    if($('#obituary').prop('files').length > 0) {
        obituaryFile =$('#obituary').prop('files')[0];
        formdata.append("obituary", obituaryFile);
    }

    if($('#event').prop('files').length > 0) {
        eventFile =$('#event').prop('files')[0];
        formdata.append("event", eventFile);
    }

    if($('#comment').prop('files').length > 0) {
        commentFile =$('#comment').prop('files')[0];
        formdata.append("comment", commentFile);
    }

    if($('#image').prop('files').length > 0) {
        imageFile =$('#image').prop('files')[0];
        formdata.append("image", imageFile);
    }

    // First of all, upload files into BE repository
    jQuery.ajax({
        url: '/upload-csv',
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (data) {
            processCSV('obituary', 0, data.batch);
        },
        error: function (data) {
            $('#loaderModalCenter').modal('hide');
            $('.message-board').html('<div class="alert alert-danger" role="alert">Oops something went wrong!</div>');
        },
    });

});

function processCSV(type = 'obituary', offset = 0, batch) {
    $.ajax({
        type: 'POST',
        url: '/process-csv',
        data: {'type' : type, 'offset' : offset, 'batch' : batch},
        success: function (data) {

            if (data.status === 'done') {
                loadIndicators();
                removeCsvFiles(data.batch);
            } else {
                processCSV(data.type, data.offset, data.batch);
            }
        },
        error: function (data) {
            $('#loaderModalCenter').modal('hide');
            $('.message-board').html('<div class="alert alert-danger" role="alert">Oops something went wrong!</div>');
        },
    });
}


function removeCsvFiles(batch)
{
    $.ajax({
        type: 'POST',
        url: '/remove-csv',
        data: {'batch' : batch},
        success: function (data) {
            $('#loaderModalCenter').modal('hide');
            $('.message-board').html('<div class="alert alert-success" role="alert">CSV files has been uploaded successfully!</div>');
        },
        error: function (data) {
            $('#loaderModalCenter').modal('hide');
            $('.message-board').html('<div class="alert alert-danger" role="alert">Oops something went wrong! We can not clear uploaded csv files. </div>');
        },
    });
}


function recursively_sync_all_records() {
    $.ajax({
        type: 'POST',
        url: '/sync/all',
        success: function (data) {
            if (data === 'done') {
                $('#loaderModalCenter').modal('hide');
                location.reload()
            } else if (data === 'in_progress') {
                recursively_sync_all_records();
            }  else {
                recursively_sync_all_records();
            }
        },
        error: function(xhr, status, error) {
            recursively_sync_all_records();
        }
    });
}

$( document ).ready(function() {
    loadIndicators();
});

function loadIndicators()
{
    $.ajax({
        type: 'POST',
        url: '/loader',
        dataType:"json",
        success: function (data) {
            data = data.data;
            $('.badge-total-records').text(data.total);
            $('.badge-synced-records').text(data.synced);
            $('.badge-pending-records').text(data.pending);
        }
    });
}
